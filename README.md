PRO:  
kotlin the multiplatform language  
compile to java and javascript  
multi paradigm language  
doesn't tell you what to do  
you tell what you want to do  
doesn't punish for imperative code  
static typed language  
extremely fluent, don't need to fight with the code  


`kotlinc sample.kt` for to compileto kotlin  
`kotlinc-jvm sample.kt` to compile to java  

to run use `kotlin sampleKt`  
to run using java `java -classpath . SampleKt`  
`kotlinc-jvm` for live coding  

for scripts use `.kts` extension, this doesn't require bloat  
`kotlinc-jvm -script sample.kts` running a script

expression is better than statement, expression returns an object

there is no void in Kotlin

There is Unit that represents Void
