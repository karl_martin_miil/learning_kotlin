package logic

import models.Snake

class SnakeGame {
    val snake: Snake = Snake()
    val board: Board = Board()
}
