package kata

import java.util.function.BiFunction

object EnoughIsEnough {
    fun deleteNth(elements: IntArray, maxOcurrences: Int):IntArray {
        val numOfReplaced: Int = 0
        val count: MutableMap<Int, Int> = HashMap()
        val result: IntArray = IntArray(elements.size)
        for ((i, element) in elements.withIndex()) {
            if (!count.contains(element)) {
                count[element] = 1
                result[i] = element
            }
            else if (count[element]?.compareTo(maxOcurrences)!! < 0) {
                count.computeIfPresent(element) { _, y -> y + 1 }
                count.putIfAbsent(element, 1)
            }
        }
        result.
        return elements
    }
}
