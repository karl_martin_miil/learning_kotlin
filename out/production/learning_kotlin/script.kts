println("hello")

// val is like a const
// var is a immutable variable, don't use it
val prefix: String = "Hello"
val name = "Karl"

println("$prefix $name, your name has ${name.length} chars")


fun greet(prefix: String = "Howdy", name: String): String = "$prefix $name"

val message = greet(prefix, name)
println(message)

//Kotlin has default arguments for functions
println(greet(name = name))


fun createPerson(name: String, age: Int, height: Int, weight: Int) {
    println("called... $name $age $height $weight")
}

createPerson(name = "Sam", age = 12, height = 100, weight = 50)

//imperative easiness
println()
for (x in 1..10) {
    print(x)
}
println()
for (x in 1.until(10)) {
    print(x)
}
println()
for (x in 1 until 10) {
    print(x)
}
println()

val names = listOf("Tom", "Jerry")

//destructuring
//entry contains index and value
for ((i, name) in names.withIndex()) {
    println(i)
    println(name)
}

// when?
fun process(input: Any) = when (input) {
    1 -> "one"
    in 13..19 -> "teen"
    is String -> "got a string"
    else -> "whatever"
}

println(process("hello"))
println(process(1))
println(process(14))
println(process(1.5))

// smart casting
fun processed(input: Any): String {
    // can't do this in here
    // println(input.length)

    // but here it's smart casted
    return when (input) {
        1 -> "one"
        in 13..19 -> "teen"
        is String -> "got a string of length ${input.length}"
        else -> "whatever"
    }
}

println(processed("hello"))


//returning null?
fun nickName(name:String): String? {
    if (name == "Robert") {
        return "Bob"
    }
    return null
}
//handling null (safe navigation)
//Elvis operator ?: 0 if is null then return 0
println(nickName("Robert")?.length ?: 0)
println(nickName("Karl")?.length ?: 0)

//can't do this unless input has String?
//println(nickName(null)?.length ?: 0)
